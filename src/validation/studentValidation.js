import Joi from "joi";

let studentValidation = Joi.object()
.keys({
  name: Joi.string().required().min(3).max(10).messages({
    "any.required": "name is required",
    "string.base": "name field must be string",
    "string.min": "name must be at least 3 character",
    "string.max": "name must be at least 10 character",
  }),//.allow(""),
  age: Joi.number()
    .required()
    //.min(18)
    //.max(60)
    .custom((value, msg) => {
      if (value >= 18) {
        return true;
      } else {
        return msg.message("age must be at least 18");
      }
    })
    .messages({
      "any.required": "age is required",
      "number.base": "age field must be number ",
      "number.min": "name must be greater than 18",
      "number.max": "name must be less than 60",
    }),
  isMarried: Joi.boolean().required().messages({
    "any.required": "isMarried is required",
    "boolean.base": "field must be a valid boolean",
  }),
  spouseName: Joi.when("isMarried", {
    is: true,
    then: Joi.string().required(),
    otherwise: Joi.string(),
  }),

  gender: Joi.string().required().valid("male", "female", "other").messages({
    "string.base": "gender must be string",
    "any.only": "gender must be either male,female,other",
  }),
  email: Joi.string()
    .required()
    .custom((value, msg) => {
      let validEmail = value.match(
        /\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}\b/
      );
      if (validEmail) {
        return true;
      } else {
        return msg.message("email is not valid");
      }
    }),
  password: Joi.string()
    .required()
    .custom((value, msg) => {
      let isValidPassword = value.match(
        /^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[!@#$%^&*()-_+=])[A-Za-z\d!@#$%^&*()-_+=]{8,15}$/
      );
      if (isValidPassword) {
        return true;
      } else {
        return msg.message(
          "password must have at least one uppercase , one lowercase, one number, one symbol, min 8 character and max 15 character "
        );
      }
    }),
  phoneNumber: Joi.number().required(),
  roll: Joi.number().required(),
  dob: Joi.string().required(),
  location: Joi.object().keys({
    country: Joi.string().required(),
    exactLocation: Joi.string().required(),
  }),
  favTeacher: Joi.array().items(Joi.string().required()),
  favSubject: Joi.array().items(
    Joi.object().keys({
      bookName: Joi.string().required(),
      bookAuthor: Joi.string().required(),
    })
  ),
})

.unknown(false);
export default studentValidation