import { Router } from "express";
import {
  createStudentController,
  deleteStudentController,
  readSpecificStudentController,
  readStudentController,
  updateStudentController,
} from "../controller/studentController.js";
import validation from "../middleware/validation.js";
import studentValidation from "../validation/studentValidation.js";


let studentRouter = Router();
studentRouter
  .route("/")
  .post(validation(studentValidation), createStudentController)
  .get(readStudentController);

studentRouter
  .route("/:id") //localhost:8000/students/2331
  .get(readSpecificStudentController)
  .patch(updateStudentController)
  .delete(deleteStudentController);
export default studentRouter;
