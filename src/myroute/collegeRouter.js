import { Router } from "express";
import { College } from "../schema/model.js";

let collegeRouter=Router()
collegeRouter
.route("/")
.post(async(req,res,next)=>{
    let data=req.body
    try {
let result=await College.create(data)
        res.json({
            success:true,
            message:"college create successfully",
            result:result,
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message,
        })
    }

})
.get(async(req,res,next)=>{
    let data=req.body
    try {
        let result=await College.find({})
        res.json({
            success:true,
            message:"college create successfully",
            result:result,
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
})
export default collegeRouter