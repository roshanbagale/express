import { Router } from "express";
import { ClassRoom } from "../schema/model.js";

let classRoomRouter=Router();
classRoomRouter
.route("/")
.post(async(req,res,next)=>{
 let data=req.body
 try {
    let result = await ClassRoom.create(data)
    res.json({
        success:true,
        message:"classRoom create successfully",
        result:result,
    })
 } catch (error) {
    res.json({
        success:false,
        message:error.message,
    })
 }
})
 .get(async(req,res,next)=>{
let data=req.body
try {
    let result=await ClassRoom.find({})
    res.json({
        success:true,
        message:"classRoom read successfully",
        result:result,
    })
} catch (error) {
    res.json({
        success:false,
        message:error.message
    })
}
 })
 export default classRoomRouter