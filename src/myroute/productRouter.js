import { Router } from "express";
import {
  createProductController,
  deleteProductController,
  readProductController,
  readSpecificProductController,
  updateProductController,
} from "../controller/productController.js";

// let canAddProduct=(role)=>{
//   return (req,res,next)=>{
//  if(role==="admin"){
//   return next()
//  }else if(role==="customer"){
//   res.status(400).json({
//     success:false,
//     message:"you are not allow to create product"
//   })
//  }
//   }
// }

// let canAddProduct=(age)=>{
//   return (req,res,next)=>{
// if(age>18){
//   next()
// }else if(age<18){
//   res.status(400).json({
//     success:false,
//     message:"you are not allow to create product"
//   })
// }
//   }
// }

// let canAddProduct=(age,isMarried)=>{
//   return (req,res,next)=>{
// if(age>18&&isMarried===true){
//   next()
// }else if(age<18&&isMarried===false){ 
//   res.status(400).json({
//     success:false,
//     message:"you are not allow to create product"
//   })
// }
//   }
// }

let productRouter = Router();
productRouter
  .route("/")
  .post(createProductController)
  //.post(canAddProduct("admin"),createProductController)
  //.post(canAddProduct(33),createProductController)
  //.post(canAddProduct("c"),createProductController)
  .get(readProductController);

productRouter
  .route("/:id")
  .get(readSpecificProductController)
  .patch(updateProductController)
  .delete(deleteProductController);
export default productRouter;
