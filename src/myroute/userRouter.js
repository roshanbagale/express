import { Router } from "express";
import {
  createUserController,
  deleteUserController,
  loginUserController,
  readSpecificUserController,
  readUserController,
  updateUserController,
} from "../controller/userController.js";

let userRouter = Router();
userRouter.route("/").post(createUserController).get(readUserController);

userRouter
  .route("/:id")
  .get(readSpecificUserController)
  .patch(updateUserController)
  .delete(deleteUserController);

  userRouter
  .route("/login")
  .post(loginUserController)
export default userRouter;
