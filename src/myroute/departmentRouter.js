import { Router } from "express";
import { Department } from "../schema/model.js";

let departmentRouter = Router()
departmentRouter
.route("/")
.post(async(req,res,next)=>{
    let data=req.body
    try {
        let result=await Department.create(data)
        res.json({
            success:true,
            message:"department create successfully",
            result:result,
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message,
        })
    }
})
.get(async(req,res,next)=>{
    let data=req.body
    try {
        let result=await Department.find({})
        res.json({
            success:true,
            message:"department create successfully",
            result:result,
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message,
        })
    }
})

export default departmentRouter