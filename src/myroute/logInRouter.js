import { Router } from "express";
import { LogIn } from "../schema/model.js";

let logInRouter=Router();
logInRouter
.route("/")
.post(async(req,res,next)=>{
let data=req.body
try {
 let result=  await LogIn.create(data)
 res.json({
    success:true,
    message:"login create successfully",
    result:result,
 })
} catch (error) {
    res.json({
success:false,
message:error.message,
    })
}

})

.get(async(req,res,next)=>{
    try {
        let result=await LogIn.find({})
        res.json({
            success:true,
            message:"LogIn successfully",
            result:result,
        })

    } catch (error) {
        res.json({
            success:false,
            message:error.message,
        })
    }
})
export default logInRouter