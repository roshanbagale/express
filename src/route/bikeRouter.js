import { Router } from "express";

export let bikeRouter = Router();
/* bikeRouter
  .route("/") //localhost:8000/bike
  .post((req, res, next) => {
    console.log(req.body);
    res.json("bike post");
  })
  .get((req, res, next) => {
    res.json("bike get");
  })
  .patch((req, res, next) => {
    res.json("bike update");
  })
  .delete((req, res, next) => {
    res.json("bike delete");
  }); */

bikeRouter
  .route("/name") //localhost:8000/bike/name
  .get((req, res, next) => {
    res.json("bike name get");
  });

  /* bikeRouter
  .route("/:id")//localhost:8000/bike/1234
  .get((req,res,next)=>{
    console.log(req.params)
    //req.params={
      //id:1234
    //} 
    res.json("id")
  }) 
  */
  bikeRouter
  .route("/:id")
  .get((req,res,next)=>{
    console.log(req.params)
    /* 
    req.query={
      name="roshan"
      address="ktm"
    }
    */
    res.json(req.query)
  })

  bikeRouter
  .route("/:id/name/:id2")
  .get((req,res,next)=>{
    console.log(req.params)
    res.json(req.params)
  })


  bikeRouter
  .route("/")//localhost:8000/bike
  .post((re1,res,next)=>{
console.log("i am middleware 1")
next()
  },(req,res,next)=>{
    console.log("i am middleware 2")
    next()

  },(req,res,next)=>{
    console.log("i am middleware 3")
    res.json({
      success:true,
      message:"bike created successfully"
    })

  })

/* .post(
    (req, res, next) => {
      console.log("i am normal middleware 1");
      let error = new Error("this is my 2 error");
      next(error);
    },
    (error, req, res, next) => {
      console.log("i am error middleware 1");
      console.log(error.message)
    },
    (req, res, next) => {
      console.log("i am normal middleware 2");
      next();
    },
    (error, req, res, next) => {
      console.log("i am error middleware 2");
    },
    (req, res, next) => {
      console.log("i am normal middleware 3");
      res.json({
        success: true,
        message: "bike created successfully",
      });
    }
  )






 */
