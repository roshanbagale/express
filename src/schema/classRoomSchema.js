import { Schema } from "mongoose";
let classRoom = Schema({
  name: {
    required: true,
    type: String,
  },
  numberOfBench: {
    required: true,
    type: Number,
  },
  hasTv: {
    required: true,
    type: Boolean,
  },
});
export default classRoom