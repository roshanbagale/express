import { Schema } from "mongoose";

let logInSchema = Schema({
  username: {
    required: true,
    type: String,
  },
  password: {
    required: true,
    type: String,
  },
});
export default logInSchema
