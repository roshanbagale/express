import { Schema } from "mongoose";

let traineeSchema = Schema(
  {
    name: {
      type: String,
    },
    class: {
      type: Number,
    },
    faculty: {
      type: String,
    },
  },
  { timestamps: true }
);
export default traineeSchema;
