import { model } from "mongoose";
import bookSchema from "./bookSchema.js";
import classRoom from "./classRoomSchema.js";
import collegeSchema from "./collegeSchema.js";
import department from "./departmentSchema.js";
import studentSchema from "./studentSchema.js";
import teacherSchema from "./teacherSchema.js";
import traineeSchema from "./traineeSchema.js";
import logInSchema from "./logInSchema.js";
import productSchema from "./productSchema.js";
import reviewSchema from "./reviewSchema.js";
import userSchema from "./userSchema.js";
import webUserSchema from "./webUserSchema.js";

export let Student = model("Student", studentSchema);

export let Teacher = model("Teacher", teacherSchema);

export let Book = model("Book", bookSchema);

export let Trainee = model("Trainee", traineeSchema);

export let College = model("College", collegeSchema);

export let ClassRoom = model("ClassRoom", classRoom);

export let Department = model("Department", department);

export let LogIn = model("LogIn", logInSchema);

export let Product = model("Product", productSchema);
export let User = model("User", userSchema);

export let Review = model("Review", reviewSchema);

export let WebUser=model("WebUser",webUserSchema)