import { Schema } from "mongoose";

let userSchema = Schema({
  fullName: {
    required: true,
    type: String,
    trim:true
  },
  address: {
    type: String,
    trim:true
  },
  email:{
    unique:true,
    required:true,
    type:String,
    trim:true
  },
  password:{
    required:true,
    type:String,
    trim:true
  },
  phoneNo:{
    required:true,
    type:Number,
    trim:true
  },
  gender:{
    required:true,
    type:String,
    trim:true,
    default:"male"
  },
  dob:{
    required:true,
    type:Date,
    trim:true
    },
    isMarried:{
      type:Boolean,
      trim:true
    }
},{timestamps:true});
export default userSchema;
