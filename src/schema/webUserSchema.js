import { Schema } from "mongoose";

let webUserSchema = Schema(
  {
    fullName: {
      required: [true, "fullName filed is required."],
      type: String,
      trim: true,
    },

    email: {
      unique: true,
      required: [true, "email filed is required."],
      type: String,
      trim: true,
    },
    password: {
      required: [true, "password filed is required."],
      type: String,
      trim: true,
    },

    dob: {
      required: [true, "dob filed is required."],
      type: Date,
      trim: true,
    },
    gender: {
      required: [true, "gender filed is required."],
      type: String,
      trim: true,
      default: "male",
    },
    role: {
      required: [true, "role filed is required."],
      type: String,
      trim: true,
    },
    isVerifiedEmail: {
      // required:[true,"isVerifiedEmail filed is required."],
      type: Boolean,
      trim: true,
    },
  },
  { timestamps: true }
);
export default webUserSchema;
