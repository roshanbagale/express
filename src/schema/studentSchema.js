import { Schema } from "mongoose";
let studentSchema = Schema(
  {
    name: {
      type: String,
      //lowercase:true
      //uppercase:true
      //trim: true
      // required:[true,"name field is required"],//error field must be resemble with field name
      // minLength:[3,"name field must be more than 3 characters"],
      // maxLength:[25,"name field must be more than 25 characters"],

    },
    age:{
      type:Number
    },
    password: {
      type: String,
      
    },
    phoneNumber: {
      type: Number,
      // min:[1000000000,"phoneNumber must be of 10 digits"],
      // max:[9999999999,"phoneNumber must be of 10 digits"]
//         validate:(value)=>{
// if(value.toString().length !==10){
//   throw new Error("phoneNumber must be exact 10 digits")
// }
//         }
    },
    roll: {
      type: Number,
      // min:[400,"roll must be more than 400"],
      // max:[500,"roll must be less than 500"]
    },
    isMarried: {
      type: Boolean,
      //default: false,
    },
    spouseName: {
      type: String,
    },
    email: {
      type: String,
    },
    gender: {
      type: String,
      // validate:(value)=>{
      //   if(value==="male"||value==="female"||value==="other"){
      //   }
      //   throw new Error("gender must be either male or female or other")
      // }
    },
    dob: {
      type: Date,
      //default:new Date()
    },
    location: {
      country: {
        type: String,
      },
      exactLocation: {
        type: String,
      },
    },
    favTeacher: [
      {
        type: String,
      },
    ],
    favSubject: [
      {
        bookName: {
          type: String,
        },
        bookAuthor: {
          type: String,
        },
      },
    ],
  },
  { timestamps: true }
);
export default studentSchema;
/* 
create 
    validation
read
   
*/