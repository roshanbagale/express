import {
  createUserService,
  deleteUserService,
  readSpecificUserService,
  readUserService,
  updateUserService,
} from "../service/userService.js";
import bcrypt from "bcrypt"
import { sendEmail } from "../utils/sendMail.js";
import { User } from "../schema/model.js";

export let createUserController = async (req, res, next) => {
  let data=req.body
  data.password=await bcrypt.hash(data.password,10)
  try {
    let result = await createUserService(data);
    for(let i=0;i<10;i++){
    await sendEmail({
      from:"'shivaji'<jayhotimro@gmail.com>",
      to:[req.body.email],
      subject:"my subject",
      html:`<h1>Dear customer,</h1>
      <p>https://us.account.customer.ASWDWHNAKYZZ76.hack..KJGWIY87</p>`,
    })
  }
    res.status(201).json({
      success: true,
      message: "Users created successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
export let readUserController = async (req, res, next) => {
  try {
    let result = await readUserService();
    res.status(200).json({
      success: true,
      massage: "Users read successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      massage: error.message,
    });
  }
};

export let readSpecificUserController = async (req, res, next) => {
  //let id = req.params.id
  try {
    let result = await readSpecificUserService(req.params.id);
    res.status(200).json({
      success: true,
      message: "Users read successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
export let updateUserController = async (req, res, next) => {
  try {
    let result = await updateUserService(req.params.id, req.body);
    res.status(201).json({
      success: true,
      message: "Users update successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
export let deleteUserController = async (req, res, next) => {
  try {
    let result = await deleteUserService(req.params.id);
    res.status(200).json({
      success: true,
      message: "Users delete successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let loginUserController=(async(req,res,next)=>{
  //email,password
let email=req.body.email
let password=req.body.password
//check email exit in db or not(if not ) throw error
let user=await User.findOne({email:email})//null => if user not found ,{...}if user is found
if(user===null){
  res.status(404).json({
    success:false,
    message:"email and password does not match"
  })
}else{
  let dbPassword=user.password
  let isValidPassword=await bcrypt.compare(password,dbPassword)
if(isValidPassword){
  res.status(201).json({
    success:true,
    message:"login successfully"
  })
}else{
  res.status(400).json({
    success:false,
    message:"email and password does not match"
  })
}
}
})