import {
  createBookService,
  deleteBookService,
  readBookService,
  readSpecificBookService,
  updateBookService,
} from "../service/bookService.js";

export let createBookController = async (req, res, next) => {
  try {
    let result = await createBookService(req.body);
    res.status(201).json({
      success: true,
      message: "book create successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let readBookController = async (req, res, next) => {
  try {
    let result = await readBookService();
    res.status(200).json({
      success: true,
      message: "book read successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
export let readSpecificBookController = async (req, res, next) => {
  try {
    let result = await readSpecificBookService(req.params.id);
    res.status(200).json({
      success: true,
      message: "book read successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    })
  }
};
export let updateBookController=async(req,res,next)=>{

try {
    let result = await updateBookService(req.params.id,req.body)
res.status(201).json({
    success:true,
    message:" book update  successfully",
    result:result
})
} catch (error) {
    res.status(400).json({
        success:false,
        message:error.message
    })
}
}
export let deleteBookController=async(req,res,next)=>{
    try {
        let result =await deleteBookService(req.params.id)
res.status(200).json({
    success:true,
    message:"book delete successfully",
    result:result
})
    } catch (error) {
        res.status(400).json({
            success:false,
            message:error.message
        })
    }
}