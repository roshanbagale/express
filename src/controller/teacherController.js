import {
  createTeacherService,
  deleteTeacherService,
  readSpecificTeacherService,
  readTeacherService,
  updateTeacherService,
} from "../service/teacherService.js";

export let createTeacherController = async (req, res, next) => {
  try {
    let result = await createTeacherService(req.body);
    res.status(201).json({
      success: true,
      message: "teacher create successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let readTeacherController = async (req, res, next) => {
  let {totalDataInPage,pageNo,sort,...query}=req.query
  let limit = totalDataInPage
  let skip=(Number(pageNo-1))*Number(totalDataInPage)
  try {
    let result = await readTeacherService(query,sort,skip,limit);
    res.status(200).json({
      success: true,
      message: "teacher read successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
export let readSpecificTeacherController = async (req, res, next) => {
  try {
    let result = await readSpecificTeacherService(req.params.id);
    res.status(200).json({
      success: true,
      message: "teacher read successfully ",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
export let updateTeacherController=async(req,res,next)=>{
 try {
  let result = await updateTeacherService(req.params.id,req.body)
  res.status(201).json({
    success: true,
    message: "teacher update successfully ",
    result: result,
  });
 } catch (error) {
  res.status(400).json({
    success: false,
    message: error.message,
  });
 }
}
export let deleteTeacherController=async(req,res,next)=>{
try {
  let result = await deleteTeacherService(req.params.id)
  res.status(200).json({
    success:true,
    message:"delete teacher successfully",
    result:result
  })
} catch (error) {
  res.status(400).json({
    success:false,
    message:error.message
  })
}
}
