import { createTraineeService, deleteTraineeService, readSpecificTraineeService, readTraineeService, updateTraineeService } from "../service/traineeService.js";

  export let createTraineeController = async (req, res, next) => {
    try {
      let result = await createTraineeService(req.body);
      res.status(201).json({
        success: true,
        message: "trainee create successfully",
        result: result,
      });
    } catch (error) {
      res.status(400).json({
        success: false,
        message: error.message,
      });
    }
  };
  
  export let readTraineeController = async (req, res, next) => {
    try {
      let result = await readTraineeService();
      res.status(200).json({
        success: true,
        message: "trainee read successfully",
        result: result,
      });
    } catch (error) {
      res.status(400).json({
        success: false,
        message: error.message,
      });
    }
  };
  export let readSpecificTraineeController = async (req, res, next) => {
    try {
      let result = await readSpecificTraineeService(req.params.id);
      res.status(200).json({
        success: true,
        message: "trainee read successfully ",
        result: result,
      });
    } catch (error) {
      res.status(400).json({
        success: false,
        message: error.message,
      });
    }
  };
  export let updateTraineeController=async(req,res,next)=>{
   try {
    let result = await updateTraineeService(req.params.id,req.body)
    res.status(201).json({
      success: true,
      message: "trainee update successfully ",
      result: result,
    });
   } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
   }
  }
  export let deleteTraineeController=async(req,res,next)=>{
  try {
    let result = await deleteTraineeService(req.params.id)
    res.status(200).json({
      success:true,
      message:"delete trainee successfully",
      result:result
    })
  } catch (error) {
    res.status(400).json({
      success:false,
      message:error.message
    })
  }
  }
  