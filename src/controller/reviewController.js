import {
  createReviewService,
  deleteReviewService,
  readReviewService,
  readSpecificReviewService,
  updateReviewService,
} from "../service/reviewService.js";

export let createReviewController = async (req, res, next) => {
  try {
    let result = await createReviewService(req.body);
    res.status(201).json({
      success: true,
      message: "Reviews created successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
export let readReviewController = async (req, res, next) => {
  try {
    let result = await readReviewService();
    res.status(200).json({
      success: true,
      massage: "Reviews read successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      massage: error.message,
    });
  }
};

export let readSpecificReviewController = async (req, res, next) => {
  //let id = req.params.id
  try {
    let result = await readSpecificReviewService(req.params.id);
    res.status(200).json({
      success: true,
      message: "Reviews read successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
export let updateReviewController = async (req, res, next) => {
  try {
    let result = await updateReviewService(req.params.id, req.body);
    res.status(201).json({
      success: true,
      message: "Reviews update successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
export let deleteReviewController = async (req, res, next) => {
  try {
    let result = await deleteReviewService(req.params.id);
    res.status(200).json({
      success: true,
      message: "Reviews delete successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
