import {
  createProductService,
  deleteProductService,
  readProductService,
  readSpecificProductService,
  updateProductService,
} from "../service/productService.js";

export let createProductController = async (req, res, next) => {
  // let data = req.body;
  try {
    let result = await createProductService(req.body);
    res.status(201).json({
      success: true,
      message: "Products created successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
export let readProductController = async (req, res, next) => {
  try {
    let result = await readProductService();
    res.status(200).json({
      success: true,
      massage: "Products read successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      massage: error.message,
    });
  }
};

export let readSpecificProductController = async (req, res, next) => {
  //let id = req.params.id
  try {
    let result = await readSpecificProductService(req.params.id);
    res.status(200).json({
      success: true,
      message: "Products read successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
export let updateProductController = async (req, res, next) => {
  try {
    let result = await updateProductService(req.params.id, req.body);
    res.status(201).json({
      success: true,
      message: "Products update successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
export let deleteProductController = async (req, res, next) => {
  try {
    let result = await deleteProductService(req.params.id);
    res.status(200).json({
      success: true,
      message: "Products delete successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
