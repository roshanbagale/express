import {
  createStudentService,
  deleteStudentSpecific,
  readSpecificStudentService,
  readStudentService,
  updateStudentService,
} from "../service/studentService.js";

export let createStudentController = async (req, res, next) => {
  // let data = req.body;
  try {
    let result = await createStudentService(req.body);
    res.status(201).json({
      success: true,
      message: "Students created successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
export let readStudentController = async (req, res, next) => {
 
  let {totalDataInPage,pageNo,sort,...query} = req.query;
//  totalDataInPage=3
//pageNo=2
  let limit=totalDataInPage
  let skip=(Number(pageNo-1))*Number(totalDataInPage)
  //query={name:"roshan",age:400}
  try {
    let result = await readStudentService(query,sort,skip,limit);
    res.status(200).json({
      success: true,
      massage: "students read successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      massage: error.message,
    });
  }
};

export let readSpecificStudentController = async (req, res, next) => {
  //let id = req.params.id
  try {
    let result = await readSpecificStudentService(req.params.id);
    res.status(200).json({
      success: true,
      message: "students read successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
export let updateStudentController = async (req, res, next) => {
  try {
    let result = await updateStudentService(req.params.id, req.body);
    res.status(201).json({
      success: true,
      message: "students update successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
export let deleteStudentController = async (req, res, next) => {
  try {
    let result = await deleteStudentSpecific(req.params.id);
    res.status(200).json({
      success: true,
      message: "students delete successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};