import { Trainee } from "../schema/model.js";

export let createTraineeService = async (data) => await Trainee.create(data);
export let readTraineeService = async () => await Trainee.find({name:"ims"}).select("name class -_id");

export let readSpecificTraineeService = async (id) =>
  await Trainee.findById(id);

export let updateTraineeService = async (id, data) =>
  await Trainee.findByIdAndUpdate(id, data, {
    new: true,
  });

export let deleteTraineeService = async (id) =>
  await Trainee.findByIdAndDelete(id);
