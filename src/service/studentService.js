import { Student } from "../schema/model.js";

export let createStudentService = async (data = {}) => {
  return await Student.create(data);
};
export let readStudentService = async (
  query = {},
  sort = null,//we can use "" bug in mogoose
  skip = 0,
  limit = 10
) => {
  //return await Student.find({})

  //find has control over the object
  //select has control over the object property
  //  we can not use -(exclusive )and +(inclusive) simultaneously expect _id

  // return await Student.find({name:"Roshan"}).select(
  //     "name password phoneNumber -_id"
  // );

  //return await Student.find({}).select("-password")
  // return await Student.find(query)
  return await Student.find(query).sort(sort).skip(skip).limit(limit);
};

export let readSpecificStudentService = async (id = "") => {
  return Student.findById(id);
};
export let updateStudentService = async (id = "", data = {}) => {
  return await Student.findByIdAndUpdate(id, data, {
    new: true,
  });
};
export let deleteStudentSpecific = async (id = "") => {
  return await Student.findByIdAndDelete(id);
};
