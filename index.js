import express, { json } from "express";
import connectToMongoDB from "./src/connectDB/connectToMongoDB.js";
import bookRouter from "./src/myroute/bookRouter.js";
import classRoomRouter from "./src/myroute/classRoomRouter.js";
import collegeRouter from "./src/myroute/collegeRouter.js";
import departmentRouter from "./src/myroute/departmentRouter.js";

import logInRouter from "./src/myroute/logInRouter.js";
import productRouter from "./src/myroute/productRouter.js";
import reviewRouter from "./src/myroute/reviewRouter.js";
import studentRouter from "./src/myroute/studentRouter.js";
import teacherRouter from "./src/myroute/teacherRouter.js";
import traineeRouter from "./src/myroute/traineeRouter.js";
import userRouter from "./src/myroute/userRouter.js";
import webUserRouter from "./src/myroute/webUserRouter.js";
import { bikeRouter } from "./src/route/bikeRouter.js";
import { firstRouter } from "./src/route/firstRouter.js";
import { nameRouter } from "./src/route/nameRouter.js";
import { traineesRouter } from "./src/route/traineesRouter.js";
import fileRouter from "./src/myroute/fileRouter.js";
import { port } from "./src/constant.js";
import cors from "cors"
let expressApp = express();
connectToMongoDB()
  

/* expressApp.use(
  (req,res,next)=>{
    console.log("i am application, normal middleware 1")
    let error = new Error("i am application error")
    next(error)
  },
(err,req,res,next)=>{
  console.log("i am application, error middleware 1 ")
  console.log(err.message)
  next()
},
(req,res,next)=>{
  console.log("i am application , normal middleware 2")
  next()
}
) */

expressApp.use(express.static("./public"))
expressApp.use(cors())
expressApp.use(json());//it is done to make our application to accept json data

expressApp.use("/traineess", traineesRouter);
expressApp.use("/", firstRouter)
expressApp.use("/names",nameRouter)
expressApp.use("/bikes",bikeRouter)
expressApp.use("/students",studentRouter)
expressApp.use("/logIns",logInRouter)
expressApp.use("/teachers",teacherRouter)
expressApp.use("/trainees",traineeRouter)
expressApp.use("/colleges",collegeRouter)
expressApp.use("/classrooms",classRoomRouter)
expressApp.use("/books",bookRouter)
expressApp.use("/departments",departmentRouter)
expressApp.use("/products",productRouter)
expressApp.use("/users",userRouter)
expressApp.use("/reviews",reviewRouter)
expressApp.use("/web-users",webUserRouter)
expressApp.use("/files",fileRouter)
expressApp.listen(port, () => {
  console.log("app is listening at port 8000");
});
