/* 
status code
success (2XX)
c 201
r 200
u 201
d 200

failure (4XX)
400




question 
{
    "name": "c",
    "password":"Password@123",
    "phoneNumber": 1111111111,
    "roll": 2,
    "isMarried": false,
    "spouseName": "lkjl",
    "email": "abc@gmai",
    "gender": "male",
    "dob": "2019-2-2",
    "location": {
        "country": "nepal",
        "exactLocation": "gagalphedi"
    },
    "favTeacher": [
        "a",
        "b",
        "c",
        "nitan"
    ],



    "favSubject": [
        {
            "bookName": "javascript",
            "bookAuthor": "nitan"
        },
        {
            "bookName": "b",
            "bookAuthor": "b"
        }
    ],



}

joi validation
.string()
     value must be string
     it should not be empty("")
  .min(3)=>the field must have at least 3 character
  .max(10)=>the field must have at least 10 character

.number()
   value must be number(it does not see type of number)
   it means 21 and "21" are same
.boolean()
   value must be boolean

.required=>any(string,number,boolean)  
you must pass field  


enum=>fixed value (male,female,other)
    .valid("male","female","other")
 
through custom error   
  

object
  Joi.object().keys(

  )

array
  Joi.array().items()

.when
    if married true=>required
    if married false=>spouseName is not required


    in mongodb
    array is called  as collection(defining collection is called making model)
    object is called document(defining document is called making schema)
    it means data are save in the form of collection of document




.env
    is the file where we define variable
    we must make .env in root director
    we use uppercase convention
    every data in ,env variable are string so no need to use double quotes
    to get .env variable we  must use dotenv package 
    if you want .env variable in file first you config dotenv in the file 
    to get .env variable use process.env.VARIABLE_NAME
    in .env we store variable like
       port 
          backend port 
          mongodb port 
       url
          server url
          client url   
        credential
          email password
          secret key
        email info
           email
           app password
*/