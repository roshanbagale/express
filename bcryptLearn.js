
//"roshan"=>"$2b$10$T1P/EtPUgZPMV65ctjvB4eGS8SDvP1BYr/BanspEfdxpFxCFp14p2"
//hashing
    //encryption is possible
    //decryption is not possible
//hash code of same string is different
import bcrypt from "bcrypt"
//*******generate hash code */
// let hashCode= await bcrypt.hash("roshan",10)
// console.log(hashCode)

let isValidPassword=await bcrypt.compare(
    "roshan",//realpassword
    "$2b$10$T1P/EtPUgZPMV65ctjvB4eGS8SDvP1BYr/BanspEfdxpFxCFp14p2"//hashpassword
)

//the output is true if hashPassword is made from the password
console.log(isValidPassword)